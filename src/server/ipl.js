function matchesperyear(x){
    let obj={};
    for(let i=0;i<x.length;i++){
        if (x[i].season in obj){
            obj[x[i].season] +=1
        }else{
            obj[x[i].season] =1
        }
    }
    return obj;
}

function matchesownperyear(x){
    let obj={}
    for (i=0;i<x.length;i++){
        if(x[i].season in obj){
            if(x[i].winner in obj[x[i].season]){
                obj[x[i].season][x[i].winner] +=1
            }else{
                obj[x[i].season][x[i].winner] =1
            }
        }else{
            obj[x[i].season] ={}
            obj[x[i].season][x[i].winner] =1
        }
    }
    return obj;
}

function extraruns2016(x,y,year){
    let obj={}
    let matchid;
    for(let i=0;i<x.length;i++){
        if (x[i].season === year){
            matchid = x[i].id
        }
        for(let j=0;j<y.length;j++){
            if (matchid === y[j].match_id){
                if(y[j].bowling_team in obj){
                    obj[y[j].bowling_team] += y[j].extra_runs
                }else{
                    obj[y[j].bowling_team] = y[j].extra_runs
                }
            }
        }
    }
    return obj;
}



function topTenEcoBowlers2015(x,y,year){
    let obj = {};
    let totalRuns={};
    let totalBalls = {};
    let matchid;
    for (let i in x){
        if (x[i].season === year){
            matchid = x[i].id
        }
        for(let j in y){
            if (y[j].match_id === matchid){
                if(y[j].bowler in totalBalls && y[j].bowler in totalRuns){
                    totalBalls[y[j].bowler] +=1
                    totalRuns[y[j].bowler] += y[j].total_runs
                }else{
                    totalBalls[y[j].bowler] =1
                    totalRuns[y[j].bowler] = y[j].total_runs
                }
            }
        }
    }
    let totalOvers={};
    for (let i in totalBalls){
        if (totalBalls[i]%6 === 0){
            totalOvers[i] = totalBalls[i]/6
        }else{
            let extraBall = totalBalls[i]%6
            totalOvers[i] = (totalBalls[i]/6 + extraBall/6).toFixed(2)
        }
    }
    let bowlers = [];
    for (let i in totalRuns){
        bowlers.push([i,(totalRuns[i]/totalOvers[i]).toFixed(2)])
    }
    bowlers.sort(function (a,b){
        return a[1]-b[1]
    })
    bowlers = bowlers.slice(0,10)
    let res={};
    for(let i=0;i<bowlers.length;i++){
        if (bowlers[i] in res){
            res[bowlers[i][0]] += bowlers[i][1]
        }else{
            res[bowlers[i][0]] = bowlers[i][1]
        }
    }
    return res;
}

module.exports.matchesperyear=matchesperyear;
module.exports.matchesownperyear=matchesownperyear;
module.exports.extraruns2016=extraruns2016;
module.exports.topTenEcoBowlers2015=topTenEcoBowlers2015;




//Extra Problems 

function wonthetossandmatch(x){
    let obj={};
    for(let i=0;i<x.length;i++){
        if (x[i].toss_winner === x[i].winner){
            if (x[i].toss_winner in obj){
                obj[x[i].toss_winner] +=1
            }else{
                obj[x[i].toss_winner] =1
            }
        }     
    }
    return obj;
}


function highestplayerawards(x){
    let obj={};
    for (let i=0;i<x.length;i++){
        if (x[i].season in obj){
            if(x[i].player_of_match in obj[x[i].season]){
                obj[x[i].season][x[i].player_of_match] +=1
            }else{
                obj[x[i].season][x[i].player_of_match] =1
            }
        }else{
            obj[x[i].season] ={};
            obj[x[i].season][x[i].player_of_match] = 1
        }
    }
    let awards = {};
    for (let i in obj){
        let key = Math.max(...Object.values(obj[i]))
        awards[i]={}
        for (let j in obj[i]){
            if (obj[i][j] === key){
                awards[i][j] =key
            }
        }
    }
    return awards;
}

function superovereco(y){
    let obj = {};
    for (let i=0;i<y.length;i++){
        if (y[i].is_super_over){
            if (!(y[i].bowler in obj)){
                obj[y[i].bowler] = {runs:0,balls:0}
                obj[y[i].bowler]["runs"] += y[i].total_runs - y[i].bye_runs- y[i].legbye_runs- y[i].penalty_runs
                if (y[i].wide_runs ===0 && y[i].noball_runs === 0){
                    obj[y[i].bowler]["balls"] +=1
                }
            }else if (y[i].bowler in obj){
                obj[y[i].bowler]["runs"] += y[i].total_runs - y[i].bye_runs- y[i].legbye_runs- y[i].penalty_runs
                if (y[i].wide_runs ===0 && y[i].noball_runs === 0){
                    obj[y[i].bowler]["balls"] +=1
                }
            }
            
        }
    }
    let obj1 ={}
    for (let i in obj){
        obj1[i] = parseFloat(((obj[i]["runs"]/obj[i]["balls"])*6).toFixed(3))
    }   
    const bowlers = Object.entries(obj1).sort(function (a,b){
        return a[1]-b[1]
    })
    return bowlers[0];
}

function highestdismissals(y){
    let obj = {};
    for (let i=0;i<y.length;i++){
        if (y[i].player_dismissed !='' && y[i].player_dismissed != 'run out'&& 
        y[i].player_dismissed != "retired hurt" && y[i].player_dismissed !="hit wicket" 
        && y[i].player_dismissed != "destructing the field"){
            if (y[i].player_dismissed in obj){
                if (y[i].bowler in obj[y[i].player_dismissed]){
                    obj[y[i].player_dismissed][y[i].bowler] +=1
                }else{
                    obj[y[i].player_dismissed][y[i].bowler] =1
                }
            }else{
                obj[y[i].player_dismissed] = {}
                obj[y[i].player_dismissed][y[i].bowler] =1
            }
        }
    }
    let arr=[];
    for (let i in obj){
        for (let j in obj[i]){
            let arr1=[];
            arr1.push(i)
            arr1.push(j)
            arr1.push(obj[i][j])
            arr.push(arr1)
        }
    }
    let res = arr.sort(function (a,b){
        return b[2]-a[2]
    })
    return res[0];
}

function strikrate(x,y){
    let obj = {};
    for (let i in x){
        for (let j in y){
            if (x[i].id === y[j].match_id){
                if (y[j].batsman in obj){
                    if (x[i].season in obj[y[j].batsman]){
                        obj[y[j].batsman][x[i].season]["runs"] += y[j].batsman_runs
                        if(y[j].wide_runs ===0 && y[j].noball_runs ===0){
                            obj[y[j].batsman][x[i].season]["balls"] +=1
                        }
                    }else{
                        obj[y[j].batsman][x[i].season] = {runs:0,balls:0}
                        obj[y[j].batsman][x[i].season]["runs"] += y[j].batsman_runs
                        if(y[j].wide_runs ===0 && y[j].noball_runs ===0){
                            obj[y[j].batsman][x[i].season]["balls"] +=1
                        }
                    }
                }else{
                    obj[y[j].batsman] ={}
                    obj[y[j].batsman][x[i].season] ={runs:0,balls:0}
                    obj[y[j].batsman][x[i].season]["runs"] += y[j].batsman_runs
                        if(y[j].wide_runs ===0 && y[j].noball_runs ===0){
                            obj[y[j].batsman][x[i].season]["balls"] +=1
                        }
                }
            }
        }
    }
   let obj1 = {}
   for (let i in obj){
       let year = {};
       for (let j in obj[i]){
           year[j] = parseFloat(((obj[i][j]["runs"]/obj[i][j]["balls"])*100).toFixed(3))
       }
       obj1[i] = year;
   }
   return obj1;
}




module.exports.wonthetossandmatch=wonthetossandmatch;
module.exports.highestplayerawards=highestplayerawards;
module.exports.superovereco=superovereco;
module.exports.highestdismissals=highestdismissals;
module.exports.strikrate=strikrate;